<?php
class AdminsController extends AppController
{
	public $helper = array('Html', 'Form');

	public function beforeFilter() {
		parent::beforeFilter();
		
		if ($this->Auth->loggedIn()) {
			$this->Auth->deny('admin_login');
		} else {
			$this->Auth->allow('admin_login');
		}
	}

/**
 * Displays a view
 *
 * @return void 
 */
	public function admin_index() {
		$this->layout = 'backend';
	}

/**
 * Method admin_login for admin login
 *
 * @return void
 */
	public function admin_login() {
		$this->layout = 'backend_login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Session->setFlash(
	            __('Username or password is incorrect'),
	            'default',
	            array(),
	            'auth'
        	);
		}
	}

	public function admin_logout() {
		return $this->redirect($this->Auth->logout());
	}
}
