﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/theme.css" />
    <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
    
    <!-- Pnotify css -->
    <link rel="stylesheet" href="assets/css/pnotify.custom.min.css" />

    <!-- PAGE LEVEL STYLES -->
<link href="assets/css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
<link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
<link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
<link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
<link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
<link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />
<link rel="stylesheet" href="assets/css/magnific-popup.css" />

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
       

        <!--PAGE CONTENT -->
        <div id="content">             
            <div class="inner" style="min-height: 700px;">
                    <div class="crumbs">
                            <ul id="breadcrumbs" class="breadcrumb">
                                <li class="active">
                                    <i class="icon-home"></i>
                                    <a href="index.html" class="active">Home</a>
                                </li>
                                <li>Dashboard</li>
                            </ul>
                        </div>
                    <div class="panel1" id="inline-popups">    
                        <div class="pull-right">
                            <button class="btn btn-primary btn-grad btn-bottom" href="#" data-toggle="modal" data-target="#myModal">
                                <i class="icon-plus icon-spacing"></i>Add New Administrator
                            </button>
                            <a class="btn btn-primary btn-grad btn-bottom" href="#test-popup1" data-effect="mfp-zoom-in">
                                <i class="icon-trash icon-spacing"></i>Trush
                            </a>
                        </div>                            
                        <span class="clearfix"></span> 
                        <div class="panel panel-default">
                            <div class="panel-heading clearfix"> 
                                
                                    <div class="right-inner-addon pull-right">
                                        <i class="icon-search"></i>
                                        <input class="form-control" type="search" placeholder="Search">
                                    </div>
                             </div>
                             <div class="table-responsive">
                                <table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Last-modified</th>
                                        <th>Update</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs btn-grad" href="#">Active</a>
                                            </td>
                                            <td id="inline-popups">
                                                <a class="btn btn-ok" href="#active-modal" data-effect="mfp-zoom-in">
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </a>
                                                 <a class="btn btn-green" data-effect="mfp-zoom-in" href="#edit-modal">
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </a>
                                                <a class="btn btn-blue" data-effect="mfp-zoom-in" href="#search-modal">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </a>
                                                <a class="btn btn-red" data-effect="mfp-zoom-in" href="#test-popup">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>
                                            <td id="inline-popups">
                                                <a class="btn btn-ok">
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </a>
                                                <a class="btn btn-green">
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </a>
                                                <a class="btn btn-blue" href="#">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </a>
                                                <a class="btn btn-red" class="btn btn-red" data-effect="mfp-zoom-in" href="#test-popup">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>
                                            <td id="inline-popups">
                                                <a class="btn btn-ok">
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </a>
                                                <a class="btn btn-green">
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </a>
                                                <a class="btn btn-blue" href="#">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </a>
                                                <a class="btn btn-red" class="btn btn-red" data-effect="mfp-zoom-in" href="#test-popup">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                             <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>
                                            <td id="inline-popups">
                                                <a class="btn btn-ok">
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </a>
                                                <a class="btn btn-green">
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </a>
                                                <a class="btn btn-blue" href="#">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </a>
                                               
                                                <a class="btn btn-red" data-effect="mfp-zoom-in" href="#test-popup">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>    
                                </table> 
                            </div>                                   
                        </div>
                        <nav class="clearfix"> <!-- pagination start -->
                              <ul class="pagination pull-right">
                                    <li id="dataTables-example_previous" class="paginate_button previous disabled" aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">Previous</a>
                                    </li>
                                    <li class="paginate_button active" aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">1</a>
                                    </li>
                                    <li class="paginate_button " aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">2</a>
                                    </li>
                                    <li class="paginate_button " aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">3</a>
                                    </li>
                                    <li class="paginate_button " aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">4</a>
                                    </li>
                                    <li class="paginate_button " aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">5</a>
                                    </li>
                                    <li class="paginate_button " aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">6</a>
                                    </li>
                                    <li id="dataTables-example_next" class="paginate_button next" aria-controls="dataTables-example" tabindex="0">
                                    <a href="#">Next</a>
                                    </li>
                                </ul>
                        </nav>  <!-- pagination end -->
                </div> <!-- padding end -->        
                        
                         
            </div>

        </div>
        <!--END PAGE CONTENT -->      
    </div>

    <!--END MAIN WRAPPER -->

    

            <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <span class="button b-close class="close" data-dismiss="modal" aria-label="Close"">
                             <span>X</span>
                        </span>
                        <h4 class="modal-title" id="myModalLabel">Add New Administrator</h4>
                      </div>
                      <div class="modal-body">
                        <form action="#" id="block-validate">
                             <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">                                   
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="required2" class="form-control" placeholder="First Name"/>                      
                                    </div>
                                    <div class="col-md-6">                                    
                                        <label class="control-label">Last Name</label>
                                        <input class="form-control" placeholder="Last Name" type="text" name="required2">
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group">
                                <label class="control-label">Uername</label>
                                <input type="text" name="username2" class="form-control" placeholder="username Name"/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" id="email2" name="email2" class="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">                                   
                                        <label class="control-label">Password</label>
                                        <input type="password" id="password2" name="password2" class="form-control" />
                                    </div>
                                    <div class="col-md-6">                                    
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" id="confirm_password2" name="confirm_password2" class="form-control" />
                                    </div>
                                </div>
                            </div>    
                            <div class="form-group">
                                <label class="control-label">phone</label>
                                <input type="text" name="digits" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">mobile</label>
                                <input type="text" name="mobile" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-4">Multiple right to left select</label>

                                <div class="col-lg-8">
                                    <select data-placeholder="Your Favorite Types of Bear" multiple class="form-control chzn-select  chzn-rtl"
                                            tabindex="10">
                                        <option>American Black Bear</option>
                                        <option>Asiatic Black Bear</option>
                                        <option>Brown Bear</option>
                                        <option>Giant Panda</option>
                                        <option selected="selected">Sloth Bear</option>
                                        <option selected="selected">Polar Bear</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Address1</label>
                                <input type="text" class="form-control" placeholder="Address1" name="address1">
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">Address2</label>
                                <input type="text" class="form-control" placeholder="Address2" name="address2">
                            </div> 
                             <div class="form-group">
                                <div class="input-group input-append date" id="dp3" data-date="12-02-2012"
                                     data-date-format="dd-mm-yyyy">
                                    <input class="form-control" type="text" value="12-02-2012" readonly="" />
                                    <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Address2</label>
                                <textarea class="form-control" rows="3" name="textarea2"></textarea>
                            </div>
                            <div class="form-group">                                
                                <label class="radio-inline">
                                <input id="optionsRadiosInline1" type="radio" checked="checked" value="option1" name="optionsRadiosInline">
                                </label>
                                <label>male</label>

                                <label class="radio-inline">
                                <input id="optionsRadiosInline2" type="radio" value="option2" name="optionsRadiosInline">
                                </label>
                                <label>female</label>
                            </div>
                           
                            <div class="form-group">
                               <div class="checkbox">
                                    <label>
										<input class="uniform" type="checkbox" checked="checked" value="option1">Checked checkbox
                                    </label>
                                </div>
                                
                            </div>
                             <div class="form-group">
                                        <div class="make-switch" data-on="primary" data-off="info">
                                <input type="checkbox" checked="checked" />
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="clearfix">                               
                                    <div class="pull-right">
                                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             <button class="btn btn-primary btn-grad" type="submit">
                                             Save changes
                                         </button>
                                    </div>
                                 </div> 
                               
                            </div>     
                        </form> 
                             </div>   

                      </div>
                      
                    </div>
                  </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-mini">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title branded-color" id="myModalLabel">Update Profile picture</h4>
                      </div>
                      <div class="modal-body text-center">
                        <input type="file" class="upload" />                       
                      </div> 
                      <div class="modal-footer">
                        <button class="btn btn-primary btn-grad btn-bottom" href="#">
                            <i class="icon-folder-open icon-spacing"></i>  Upload
                       </button>
                      </div>                    
                    </div>
                  </div>
                </div>

                     <!-- DELETE POP UP MODAL -->
               
                <!-- Popup itself -->
            <div id="test-popup" class="white-popup mfp-with-anim mfp-hide">
                <h3 class="text-center popup-heading">Are u sure want to Delete</h3>
                   <div CLASS="text-center"> 
                        <button class="btn btn-default close-btn" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary btn-grad" type="submit"> Save changes </button>
                   </div> 
            </div>
                    
             <div id="test-popup1" class="white-popup mfp-with-anim mfp-hide">
                <h3 class="text-center popup-heading">Are u sure want to Trush</h3>
                   <div CLASS="text-center"> 
                        <button class="btn btn-default close-btn" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary btn-grad" type="submit"> Save changes </button>
                   </div> 
            </div>
                    <!-- active modal -->
             <div id="active-modal" class="white-popup mfp-with-anim mfp-hide">
                <h3 class="text-center popup-heading">Are u sure</h3>
                   <div CLASS="text-center"> 
                        <button class="btn btn-default close-btn" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary btn-grad" type="submit"> Save changes </button>
                   </div> 
            </div>

                    <!-- search modal -->
             <div id="search-modal" class="white-popup mfp-with-anim mfp-hide">
                <div class="table-responsive">
                                <table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Last-modified</th>
                                        <th>Update</th>
                                        <th>Status</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs btn-grad" href="#">Active</a>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                             <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                    </tbody>    
                                </table> 
                            </div>                   
            </div>

                     <!-- search modal -->
             <div id="edit-modal" class="white-popup mfp-with-anim mfp-hide">
                <div class="table-responsive">
                                <table class="table table-bordered table-striped table-bordered table-hover new-addministartor-table">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Last-modified</th>
                                        <th>Update</th>
                                        <th>Status</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs btn-grad" href="#">Active</a>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                             <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>Mark</td>
                                            <td>
                                                Lorem Ipsum has been the industry's standard dummy text ever 1500s,
                                            </td>
                                             <td>
                                                <a class="btn btn-danger btn-xs btn-grad" href="#"> Inactive</a>
                                            </td>                                            
                                        </tr>
                                    </tbody>    
                                </table> 
                            </div>                   
            </div>

     <!-- GLOBAL SCRIPTS -->
    <script src="assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->


      <!-- PAGE LEVEL SCRIPT-->
 <script src="assets/js/jquery-ui.min.js"></script>
 <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="assets/plugins/daterangepicker/moment.min.js"></script>
<script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
<script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
       <script src="assets/js/formsInit.js"></script>
    
    <!-- validation start -->
        <script src="assets/plugins/validationengine/js/jquery.validationEngine.js"></script>
    <script src="assets/plugins/validationengine/js/languages/jquery.validationEngine-en.js"></script>
    <script src="assets/plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
    <script src="assets/js/validationInit.js"></script>
    <!-- validation end -->
     <script src="assets/js/jquery.magnific-popup.js"></script>

   <!-- <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
     END GLOBAL SCRIPTS 

 <!-- Pnotify js -->
<script src="assets/js/pnotify.custom.min.js"></script>
     
    <script>
    $(document).ready(function(){
        $(".user-img").mouseenter(function(){
            $(".edit-pencil").toggleClass("hide");
            
        });
        $(".user-img").mouseleave(function(){
            $(".edit-pencil").toggleClass("hide");
            
        });


        $(".accordion-toggle").click(function(){
            $(".icon-angle-right").toggleClass("icon-angle-down");
        });
        
  });

    // Inline popups
// Inline popups
$('#inline-popups').magnificPopup({
  delegate: 'a',
  removalDelay: 800, //delay removal by X to allow out-animation
  callbacks: {
    beforeOpen: function() {
       this.st.mainClass = this.st.el.attr('data-effect');
    }
  },
  midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});


// Image popups
$('#image-popups').magnificPopup({
  delegate: 'a',
  type: 'image',
  removalDelay: 800, //delay removal by X to allow out-animation
  callbacks: {
    beforeOpen: function() {
      // just a hack that adds mfp-anim class to markup 
       this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
       this.st.mainClass = this.st.el.attr('data-effect');
    }
  },
  closeOnContentClick: true,
  midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});


// Hinge effect popup
$('a.hinge').magnificPopup({
  mainClass: 'mfp-with-fade',
  removalDelay: 500, //delay removal by X to allow out-animation
  callbacks: {
    beforeClose: function() {
        this.content.addClass('hinge');
    }, 
    close: function() {
        this.content.removeClass('hinge'); 
    }
  },
  midClick: true
});

            $(function () { formInit(); });
            $(function () { formValidation(); });
    </script>
   
    <!-- END PAGE LEVEL SCRIPTS -->


</body>

    <!-- END BODY -->
</html>
