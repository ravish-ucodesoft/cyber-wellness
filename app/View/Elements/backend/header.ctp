<div id="top">
    <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">
        <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
            <i class="icon-align-justify"></i>
        </a>
        <header class="navbar-header">
            <a href="index.html" class="navbar-brand">
                <?php echo $this->Html->image('logo.png', array('width' => 130, 'height' => 40)); ?>
            </a>
        </header>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle text-white" data-toggle="dropdown" href="#">
                    <span class="text-white admin-text">Welcome Admin</span>
                    <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="icon-user"></i> User Profile </a>
                    </li>
                    <li><a href="#"><i class="icon-gear"></i> Settings </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <?php
                            echo $this->Html->link(
                                '<i class="icon-signout"></i> Logout',
                                '/admin/admins/logout',
                                array('escape' => FALSE)
                            );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>