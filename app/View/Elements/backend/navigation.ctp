<div id="left" >
    <div class="media user-media well-small">
        <a class="user-link" href="#">
            <?php echo $this->Html->image('backend/user.gif', array('class' => 'media-object img-thumbnail user-img')); ?>
            <span class="edit-pencil hide">
                <i class="glyphicon glyphicon-pencil pencil-position"></i>
            </span>
        </a>
        <br />
        <div class="media-body">
            <h5 class="media-heading"> Joe Romlin</h5>
            <ul class="list-unstyled user-info">
                <li>
                     <a class="btn btn-success btn-xs btn-circle" style="width: 10px;height: 12px;"></a> Online
                </li>
            </ul>
        </div>
        <br />
    </div>

    <ul id="menu" class="collapse">

        
        <li class="panel active">
            <a href="index.html" >
                <i class="icon-table"></i>  Dashboard                      
            </a>                   
        </li>
        


        <li class="panel ">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                <i class="icon-user"> </i> Administrators

                <span class="pull-right">
                  <i class="icon-angle-right"></i>
                </span>
               &nbsp; <span class="label label-default">10</span>&nbsp;
            </a>
            <ul class="collapse" id="component-nav">
               
                <li class=""><a href="button.html"><i class="icon-angle-right"></i> Buttons </a></li>
                 <li class=""><a href="icon.html"><i class="icon-angle-right"></i> Icons </a></li>
                <li class=""><a href="progress.html"><i class="icon-angle-right"></i> Progress </a></li>
                <li class=""><a href="tabs_panels.html"><i class="icon-angle-right"></i> Tabs & Panels </a></li>
                <li class=""><a href="notifications.html"><i class="icon-angle-right"></i> Notification </a></li>
                 <li class=""><a href="more_notifications.html"><i class="icon-angle-right"></i> More Notification </a></li>
                <li class=""><a href="modals.html"><i class="icon-angle-right"></i> Modals </a></li>
                  <li class=""><a href="wizard.html"><i class="icon-angle-right"></i> Wizard </a></li>
                 <li class=""><a href="sliders.html"><i class="icon-angle-right"></i> Sliders </a></li>
                <li class=""><a href="typography.html"><i class="icon-angle-right"></i> Typography </a></li>
            </ul>
        </li>
        <li class="panel active">
            <a href="index.html" >
                <i class="icon-file"></i>  Static CMS Pages                     
            </a>                   
        </li>
        <li class="panel active">
            <a href="index.html" >
                <i class="icon-user"></i>  Customers                     
            </a>                   
        </li>
        <li class="panel active">
            <a href="index.html" >
                <i class="icon-envelope"></i>  Email Templates                    
            </a>                   
        </li>
        <li class="panel active">
            <a href="index.html" >
                <i class="icon-envelope"></i>  Messages                     
            </a>                   
        </li>

    </ul>

</div>