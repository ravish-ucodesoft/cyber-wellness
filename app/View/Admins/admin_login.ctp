<div class="text-center">
   <?php echo $this->Html->image('logo.png'); ?>
</div>
<div class="tab-content">
   <div id="login" class="tab-pane active">
      <?php  echo $this->Form->create('User', array(
              'method' => 'post',
              'class' => 'form-signin',
              'id' => 'block-validate'
            ));
      ?>
         <div class="form-group">
            <?php 
               echo $this->Session->flash();
               echo $this->Session->flash('auth');
            ?>
         </div>
         <div class="form-group">
            <label>Email</label>
            <?php echo $this->Form->input('email', array(
                         'placeholder' => 'email',
                         'class' => 'form-control',
                         'div' => false,
                         'label' => false
                       )
                     );
            ?>
         </div>
         <div class="form-group">
           <label>Password</label>
           <?php echo $this->Form->input('password', array(
                           'placeholder' => 'password',
                           'class' => 'form-control',
                           'label' => false,
                           'div' => false
                        )
                     );
            ?>
         </div>
         <div class="clearfix">
           <div class="pull-left">
              <div class="checkbox remember-me">
                 <label>
                     <input type="checkbox" value="option1" checked="checked" />                                 
                     Keep me logged in                           
                 </label>
               </div>
           </div>
           <div class="pull-right">
               <button class="btn btn-primary" type="submit">Sign in</button>
           </div>
         </div>
      <?php echo $this->Form->end(); ?>
   </div>
   <div id="forgot" class="tab-pane">
      <form action="index.html" class="form-signin" id="block-validate">
         <h4 class="text-muted text-center btn-rect">Enter your valid e-mail</h4>
         <div class="form-group">
            <input type="email"  required="required" placeholder="Your E-mail"  class="form-control" name="email2"/>
         </div>                 
         <button class="btn text-muted text-center btn-success" type="submit">Recover Password</button>
      </form>
   </div>
</div>
<div class="text-center footer">
   <ul class="list-inline">
      <li><a class="text-muted" href="#login" data-toggle="tab" id="adminlogin">Login</a></li>
      <li><a class="text-muted" href="#forgot" data-toggle="tab" id="forgorpassword">Forgot Password</a></li>
   </ul>
</div>