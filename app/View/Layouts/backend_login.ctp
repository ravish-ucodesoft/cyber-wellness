<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
	    <meta charset="UTF-8" />
	    <title></title>
	    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
	    
	    <!-- GLOBAL STYLES -->
	    <?php
	    	echo $this->Html->css(
	    			array(
	    				'bootstrap/css/bootstrap',
	    				'backend/login/login',
	    				'backend/login/magic',
	    				'backend/uniform/uniform.default',
	    				'backend/validationengine/validationEngine.jquery'
	    			),
                    null,array('inline' => false)
	    		);
	    	echo $this->fetch('meta');
          	echo $this->fetch('css');
	    ?>
	</head>
	<body class="padTop53">
		<div class="login-page">
    		<div class="container">
    			<?php echo $this->fetch('content'); ?>
    		</div>
    	</div>
	    <?php echo $this->Html->script(
	    			array(
	    				'jquery1.11.0-min',
	    				'bootstrap/bootstrap.min',
	    				'backend/login/login',
	    				'backend/validationengine/jquery.validationEngine',
	    				'backend/validationengine/jquery.validationEngine-en',
	    				'backend/jquery.validate.min',
	    				'backend/validationInit'
	    			)
	    	); 
	    	echo $this->fetch('script');
	    ?>
	    <script>
		    $(function () { formValidation(); });
	        $(function () { formInit(); });
	    </script>
	</body>
</html>