<!DOCTYPE html>
	<!-- BEGIN HEAD -->
	<head>
	    <meta charset="UTF-8" />
	    <title></title>
	    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
	    
	    <!-- GLOBAL STYLES -->
	    <?php
	    	echo $this->Html->css(
	    			array(
	    				'bootstrap/css/bootstrap',
	    				'backend/main',
	    				'backend/theme',
	    				'backend/MoneAdmin',
	    				'bootstrap/css/font-awesome',
	    				'backend/layout2',
	    				'backend/float/examples',
	    				'backend/timeline/timeline'
	    			),
                    null,array('inline' => false)
	    		);
	    	echo $this->fetch('meta');
          	echo $this->fetch('css');
	    ?>
	</head>
	<body class="padTop53 " >
		<div id="wrap" >
	        <?php echo $this->element('backend/header'); 
	        	echo $this->element('backend/navigation');
	        ?>
	        <!--PAGE CONTENT -->
	        <div id="content">
	        	<?php echo $this->fetch('content'); ?>
	        </div>
	        <!--END PAGE CONTENT -->      
	    </div>
	    <div id="footer">
	        <p>&copy;  CyberWellness &nbsp;2015 &nbsp;</p>
	    </div>

	    <?php echo $this->Html->script(
	    			array(
	    				'jquery1.11.0-min',
	    				'bootstrap/bootstrap.min',
	    				'backend/common'
	    			)
	    	); 
	    	echo $this->fetch('script');
	    ?>
	</body>
</html>