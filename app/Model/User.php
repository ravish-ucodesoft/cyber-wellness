<?php
class User extends AppModel {
	var $name = 'User';
	public $validate = array(
            'email' => array(
                    'email' => array(
                            'rule'     => 'email',
                            'required' => true,
                            'allowEmpty' => false,
                            'message' => 'invaild email'
                        )
                )
        );
}